import { config, localToken } from '../config/config';


export const  loginServices = async  (data) => {
      const result = await config.METHODS.POST('login', data)
      return result;
   }
   
export const logoutServices = async () => {
   
      const result = await config.METHODS.GET('logout', { headers: { Authorization: `Bearer ${localToken()}`}}).then(data => data.data);
   
      localStorage.removeItem('token');
   
      return result;
}

export const userAuthServices = async () => {
   
   const result = await config.METHODS.GET('profiles', { headers: { Authorization: `Bearer ${localToken()}`}}).then(data => data.data);
                 
   return result;
}
export const userGetServices = async () => {
   
   const result = await config.METHODS.GET('user', { headers: { Authorization: `Bearer ${localToken()}`}}).then(data => data.data);
                 
   return result;
}

export const userDeleteServices = async (id) => {
   
   const result = await config.METHODS.DELETE(`user/${id}`, { headers: { Authorization: `Bearer ${localToken()}`}}).then(data => data.data);
                 
   return result;
}

export const userEditServices = async (id, data ) => {
   const {name,email, phone, password, } = data
   const newData = {name,email, phone, password };
   const result = await config.METHODS.PUT(`user/${id}`, newData, { headers: { Authorization: `Bearer ${localToken()}`}}).then(data => data.data);

   return result;
}