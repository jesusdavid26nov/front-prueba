import { config, localToken } from '../config/config';


export const  articleGetService = async  () => {
      const result = await config.METHODS.GET('article', { headers: { Authorization: `Bearer ${localToken()}`}}).then(data => data.data);
     return result;
}
   