import React, { useContext } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";
import { AuthContext } from '../auth/AuthContext';
import { LoginScreen } from '../components/login/LoginScreen';
import { RegisterScreen } from '../components/register/RegisterScreen';

import { DashboardRoutes } from './DashboardRoutes';
import { PrivateRoute } from './PrivateRoute';
import { PublicRoute } from './PublicRoute';

export const AppRouters = () => {
 
  const { token } = useContext(AuthContext);


    return (
      <Router>
          <div>
            <Switch>
                <Route exact path="/register "component={ RegisterScreen } />
                <PublicRoute exact  path="/login"  component={ LoginScreen } isAuthenticated={ token }/>
                <PrivateRoute  path="/"  component={ DashboardRoutes } isAuthenticated={ token }  />
        
            </Switch>
          </div>
      </Router>
    )
}
