// import React, { useContext, useEffect } from 'react'
import { Redirect, Route, Switch } from 'react-router'
// import { UserContext } from '../auth/UserContext'
import { ArticleScreen } from '../components/article/ArticleScreen'
import { ArticlesScreen } from '../components/articles/ArticlesScreen'
import { NavbarScreen } from '../components/navbar/NavbarScreen'
import { UsersScreen } from '../components/users/UsersScreen'
// import { userAuthServices } from '../services/user.services'


export const DashboardRoutes = () => {
 
    return (
        <>
        <NavbarScreen/>
        <div>
  
          <Switch>
               <Route exact  path="/articles" component={ ArticlesScreen }></Route>
               <Route exact  path="/article/:id" component={ ArticleScreen }></Route>
               <Route exact  path="/users" component={ UsersScreen }></Route>
               <Redirect to="/articles"/>
          </Switch>
        </div>
    </>
    )
}
