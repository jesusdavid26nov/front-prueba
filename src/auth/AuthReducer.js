import { types } from "../types/type";

export const authReducer = (state = {}, action ) => {
     switch (action.type) {
         case types.login:
             
            return {
                ...action.payload,
            }

        case types.logout:
             
                return false;
         
     
         default:
             return state;
     }
}

export const userReducer = (state = false, action ) => {
    switch (action.type) {
        case types.admin:
            
           return true

       case types.user:
            
           return false
        
    
        default:
            return state;
    }
}