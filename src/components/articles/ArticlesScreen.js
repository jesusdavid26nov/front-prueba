import React, { useEffect, useState } from 'react'
import { articleGetService } from '../../services/articles.services'

export const ArticlesScreen =  ({history}) => {
    const [article, setArticle] = useState([])
   
    useEffect(() => {
        async function fetchData() {
            
            const articles =  await articleGetService();
            setArticle(
                articles
            )
        }
        fetchData()
    }, [])
    const  handleArticle = (slug, id) => {
        history.replace(`${slug}${id}`)
    }

    return (

        <div className=" flex flex-wrap  items-center justify-center">
            {

             article.map((key, i) => {
                 return (
                    <div className="m-5"  key={ i }>
                    <div className="flex flex-col max-w-xs bg-white px-7 shadow-2xl  py-6 rounded-xl space-y-2 items-center">
                    <h3 className="font-serif font-bold text-gray-900 text-xl">{ key.title }</h3>
                    <img className="w-full rounded-md" src="https://images.unsplash.com/photo-1505740420928-5e560c06d30e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80" alt="motivation" />
                    <p className="text-center leading-relaxed">{key.descriptionOne}</p>
                    <span className="text-center">{key.category}</span>
                    <button className="px-5 py-2  bg-gray-900 rounded-md text-white text-sm focus:border-transparent" onClick={ () => handleArticle(key.slug, key.id )}>leer mas</button>
                    </div>
                   </div>
                 )
             })
            
            }

            
      </div>
    )
}
