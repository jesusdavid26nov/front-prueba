import React from 'react'
import { ArticlesScreen } from '../articles/ArticlesScreen'
export const ArticleScreen = () => {
    return (
        <div className=" flex flex-wrap ">
            <div className="min-h-screen bg-white">
                <div className="h-screen flex flex-col w-1/3 bg-gray-50 overflow-auto">
                    <ArticlesScreen/>
                </div>
            </div>
        </div>
    )
}