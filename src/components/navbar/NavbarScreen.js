import React from 'react'
import {  NavLink } from 'react-router-dom'
import { logoutServices } from '../../services/user.services';
import { createBrowserHistory } from 'history';

export const NavbarScreen = () => {
 
    const handleLogout = async () => {

        await logoutServices();
        createBrowserHistory().push('/login');
    };

    return (
        <>
        <nav className=" flex items-center justify-between shadow-md bg-gray-800 flex-wrap text-white p-2 mx-auto">
            <div className="flex items-center">
                <div className="space-x-4 ml-8  font-bold  ">
                           <NavLink 
                            activeClassName="active"
                            exact
                            className="hover:text-green-500"
                            to="/Articles"
                        >
                            Articulos
                        </NavLink>
                       

                        <NavLink 
                            activeClassName="active"
                            exact
                            className="hover:text-green-500"
                            
                            to="/users"
                        >
                            Usuarios
                        </NavLink>
                 </div>
            </div>
            
            <div className="flex space-x-4">

                <button className="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded  rounded-full" onClick={ handleLogout }>
                    Logout
                </button>
            </div>
        </nav>
      </>
    )
}
