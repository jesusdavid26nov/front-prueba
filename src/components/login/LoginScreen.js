/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useContext } from 'react'
import { Link } from 'react-router-dom';
import { AuthContext } from '../../auth/AuthContext';
import { useForm } from '../../hooks/useForm';
import { loginServices } from '../../services/user.services';
import { types } from '../../types/type';
// import { types } from '../../types/type';

export const LoginScreen = () => {
    const { dispatch } = useContext(AuthContext);

    const [values, handleInputChange, reset] = useForm({
        email: '',
        password: ''
    })
    const { email, password } = values;

    const handleLogin = async (e) =>{

      e.preventDefault();
      
      const { data } = await loginServices({ email, password });
      dispatch({
        type: types.login,
        payload: {
            token: data.access_token 
        }
      })
      localStorage.setItem('token', data.access_token );

      reset();

    }

    return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
        <div className="max-w-md w-full space-y-8">
            <div>
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
                Iniciar Sesion
            </h2>
            </div>
            <form className="mt-8 space-y-6" onSubmit={ handleLogin } >
            <input type="hidden" name="remember" value="true"></input>
            <div className="rounded-md shadow-sm -space-y-px">
                <div>
                <label className="sr-only">Email address</label>
                <input id="email-address" name="email"  value={ email } onChange={ handleInputChange }  type="email" autoComplete="off" required className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm" placeholder="Email address"></input>
                </div>
                <div>
                <label  className="sr-only">Password</label>
                <input id="password" name="password" type="password" value={ password } onChange={handleInputChange} autoComplete="off" required className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm" placeholder="Password"></input>
                </div>
            </div>

            <div className="flex items-center justify-between">
                <div className="flex items-center">
                <input id="remember_me" name="remember_me" type="checkbox" className="h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded"></input>
                <label  className="ml-2 block text-sm text-gray-900">
                    Remember me
                </label>
                </div>
                <div className="text-sm">
                  <Link to="/register" className="font-medium text-gray-700  hover:text-gray-400">
                       Registrarse
                   </Link>
                </div>
            </div>

            <div>
                <button type="submit" className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-gray-800 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                    <svg className="h-5 w-5 text-gray-500 group-hover:text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fillRule="evenodd" d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z" clipRule="evenodd" />
                    </svg>
                </span>
                Sign in
                </button>
            </div>
            </form>
        </div>
    </div>
    )
}
