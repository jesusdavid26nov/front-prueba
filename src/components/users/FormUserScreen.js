/* eslint-disable valid-typeof */
import React from 'react'
import { useForm } from '../../hooks/useForm'


export const FormUserScreen = ({ setFormUser, title,values, handleInputChange, handleButtonSend}) => {
   
    const { name, email, phone, password } = values
    console.log(values);

   
    return (
     <div className="max-w-md mx-auto my-10 bg-white p-2 rounded-md shadow-2xl">
        <button class="bg-gray-400 w-6 float-right h-6 p-2 text-sm font-bold tracking-wider text-white rounded-full hover:bg-blue-600 inline-flex items-center justify-center" onClick={ () => setFormUser(({show}) => !show)}>
              <div>X</div>
        </button>
        <div className="text-center">
            <h1 className="my-3 text-3xl font-semibold text-gray-700 dark:text-gray-200">{ title }</h1>
        </div>
        <div className="m-7">
            <form id="form" onSubmit={ handleButtonSend }>

                <input type="hidden" name="apikey" value="YOUR_ACCESS_KEY_HERE"/>
                <input type="hidden" name="subject" value="New Submission from Web3Forms"/>


                <div className="mb-6">
                    <label htmlFor="name" className="block mb-2 text-sm text-gray-600 dark:text-gray-400">Nombre</label>
                    <input type="text" value={ name } name="name" id="name" placeholder="John Doe" onChange={handleInputChange} required className="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                </div>
                <div className="mb-6">
                    <label htmlFor="email" className="block mb-2 text-sm text-gray-600 dark:text-gray-400">Email Address</label>
                    <input type="email" value={ email } name="email" id="email" placeholder="you@company.com" onChange={handleInputChange} required className="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                </div>
                <div className="mb-6">

                    <label htmlFor="phone" className="text-sm text-gray-600 dark:text-gray-400">Phone Number</label>
                    <input type="text" name="phone" value={ phone } id="phone" placeholder="+1 (555) 1234-567" onChange={handleInputChange} required className="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                </div>
                <div className="mb-6">
                    <label htmlFor="password" className="block mb-2 text-sm text-gray-600 dark:text-gray-400">Password</label>
                    <input type="password" value={ password} name="password" id="password" placeholder="password" onChange={handleInputChange} className="w-full px-3 py-2 placeholder-gray-300 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-indigo-100 focus:border-indigo-300 dark:bg-gray-700 dark:text-white dark:placeholder-gray-500 dark:border-gray-600 dark:focus:ring-gray-900 dark:focus:border-gray-500" />
                </div>
                <div className="mb-6">
                    <button type="submit" className="w-full px-2 py-2 text-white bg-indigo-500 rounded-md focus:bg-indigo-600 focus:outline-none">enviar</button>
                </div>
                <p className="text-base text-center text-gray-400" id="result">
                </p>
            </form>
        </div>
   </div>
    )
}
