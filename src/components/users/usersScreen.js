/* eslint-disable jsx-a11y/alt-text */
import React, { useEffect, useState } from 'react'
import { useForm } from '../../hooks/useForm';
import { userDeleteServices, userGetServices, userEditServices } from '../../services/user.services';
import { FormUserScreen } from './FormUserScreen';

export const UsersScreen = () => {
    const [user, setUser] = useState([]);
    const [{show, title}, setFormUser] = useState({title: '', show: false});
    const [getDelete, setDelete] = useState({})
     const [IdSate, setIdSate] = useState(0)
   const [getUsers, setUsers] = useState({})


    useEffect(() => {
        async function fetchData() {
            
            const users =  await userGetServices();
            setUser(
                users
            )
        }
        fetchData()
    }, [getDelete])

    const handleCard = () => {
        setUsers({
            name:'',
            email: '',
            phone: '',
            password: ''
           })
        setFormUser({
            title: 'Crear Usuario',
            show: true,
        } )
    }

    const handleEdit = (userEdit, id) => {
        setUsers(userEdit)
        setFormUser({
            title: 'Editar Usuario',
            show: true
        })
        setIdSate(id)
    }
    
    const handleDelete = async (id) => {
        
       const result = await userDeleteServices(id);
       setDelete(result)

    }

    const [values, handleInputChange, reset] = useForm(getUsers)
    console.log(values);
    const handleButtonSend = async (e) =>{
        e.preventDefault();
        
        await userEditServices({ IdSate, data: values});
    }
  
    return (

            <div className="w-screen">
               
            <div className="min-h-screen flex items-start  justify-center font-sans overflow-hidden">
                <div className="max-w-5xl m-5 lg:w-5/6 ">
                 { show ? <></> : <button className="bg-blue-500 px-3 py-2 text-lg my-2 float-right font-semibold tracking-wider text-white rounded hover:bg-blue-600" onClick={ handleCard }> Crear Usuario</button> }
                    <div className="bg-white shadow-md rounded my-6 ">
                        <table className="min-w-max w-full table-auto">
                            <thead>
                                <tr className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                                    <th className="py-3 px-6 text-left">Name</th>
                                    <th className="py-3 px-6 text-left">Email</th>
                                    <th className="py-3 px-6 text-left">Phone</th>
                                    <th className="py-3 px-6 text-left">Role</th>
                                    <th className="py-3 px-6 text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody className="text-gray-600 text-sm font-light">
                                {
                                 user.map((user, i) => {
                                     return (
                                        <tr className="border-b border-gray-200 hover:bg-gray-100" key={i}>
                                        <td className="py-3 px-6 text-left whitespace-nowrap">
                                            <div className="flex items-center">
                                                <span className="font-medium">{user.name}</span>
                                            </div>
                                        </td>
                                        <td className="py-3 px-6 text-left">
                                            <div className="flex items-center">
                                                <span>{user.email}</span>
                                            </div>
                                        </td>
                                        <td className="py-3 px-6 text-center">
                                             <div className="flex items-center">
                                                <span>{user.phone}</span>
                                            </div>
                                        </td>
                                        <td className="py-3 px-6 text-center">
                                            <div className="flex items-center">
                                                <span>{user.role}</span>
                                            </div>
                                        </td>
                                        <td className="py-3 px-6 text-center">
                                            <div className="flex item-center justify-center">
                                                <button className="w-4 mr-2 transform hover:text-purple-500 hover:scale-110" onClick={ ()=> handleEdit(user, user.id) }>
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                                    </svg>
                                                </button>
                                                <button className="w-4 mr-2 transform hover:text-purple-500 hover:scale-110" onClick={ ()=> handleDelete(user.id) }>
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                    </svg>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                     )
                                 })

                                }
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="m-6 ">
                    {
                      show ? <FormUserScreen setFormUser={setFormUser} title={title} values={values} handleInputChange={handleInputChange} handleButtonSend={handleButtonSend}/>: <></>
                    }
                    
                </div>
            </div>
            
        </div>
         

    )
}
