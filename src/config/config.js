import axios from 'axios';
import { enviroments } from './enviroments';

export const config =  {
    METHODS: {
        GET: async (url, token = {} ) => await axios.get(`${enviroments.URL_LOCAL}/${url}`, token),
        POST: async (url, data, token = {}) => await axios.post(`${enviroments.URL_LOCAL}/${url}`, data, token),
        DELETE: async (url, token = {}) => await axios.delete(`${enviroments.URL_LOCAL}/${url}`, token),
        PUT: async  (url, data, token = {}) => await axios.put(`${enviroments.URL_LOCAL}/${url}`, data, token),
    }
    
}

export const localToken = ()  => (
  localStorage.getItem('token')
);
