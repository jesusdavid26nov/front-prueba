import React, {  useReducer } from 'react'
import { AuthContext } from './auth/AuthContext'
import { authReducer } from './auth/AuthReducer';
import { localToken } from './config/config';
import { AppRouters } from './routers/AppRouters'

const init = () => (
    localToken() || false
); 


export const PruebaApp = () => {
    const [token, dispatch] = useReducer(authReducer, {}, init);
    return (
        <AuthContext.Provider value={{token, dispatch}}> 
           <AppRouters/>
        </AuthContext.Provider> 
    )
}
