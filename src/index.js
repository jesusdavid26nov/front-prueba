import React from 'react';
import ReactDOM from 'react-dom';
import { PruebaApp } from './PruebaApp';
import './index.css';

ReactDOM.render(
    <PruebaApp/>,
  document.getElementById('root')
);
