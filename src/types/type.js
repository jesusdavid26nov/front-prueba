export const types = {
    login: '[auth] login',
    logout: '[auth] logout',
    admin: 'admin',
    user: 'user'
}